# templify + Angular

Default templates for an Angular 17 project.

---

## Load this Collection

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=Angular17
```

---

## Available templates

### Component

A Angular17 component

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=Angular17/Component -t
```

### Type

A TypeScript type

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=Angular17/Type -t
```

---

Use this with [templify](https://templify.philipp-bonin.com/) to generate new files from this template.

---

by [Philipp B.](https://github.com/cophilot)
