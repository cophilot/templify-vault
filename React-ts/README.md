# templify + React + TypeScript

Default templates for a React project with TypeScript and SCSS.

---

## Load this Collection

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-ts
```

---

## Available templates

### Component

A React component with TypeScript and SCSS.

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-ts/Component -t
```

### Provider

A React context provider with TypeScript.

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-ts/Provider -t
```

### Type

A TypeScript type.

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-ts/Type -t
```

### View

A React view with TypeScript and SCSS.

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-ts/View -t
```

---

Use this with [templify](https://templify.philipp-bonin.com/) to generate new files from this template.

---

by [Philipp B.](https://github.com/cophilot)
