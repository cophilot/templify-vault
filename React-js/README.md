# templify + React + JavaScript

Default templates for a React project with JavaScript and SCSS.

---

## Load this Collection

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-js
```

---

## Available templates

### Component

A React component with JavaScript and SCSS.

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-js/Component -t
```

### Provider

A React context provider with JavaScript.

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-js/Provider -t
```

### View

A React view with JavaScript and SCSS.

```bash
tpy load https://gitlab.com/api/v4/projects/cophilot%252Ftemplify-vault/repository/tree?path=React-js/View -t
```

---

Use this with [templify](https://templify.philipp-bonin.com/) to generate new files from this template.

---

by [Philipp B.](https://github.com/cophilot)
